import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import {LoginComponent} from "./login/login.component";
import {RegistrationComponent} from "./registration/registration.component";
import {AuthComponent} from "./auth.component";
import {AuthRoutingModule} from "./auth-routing.module";
import {SharedModule} from "../shared/shared.module";
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { GoogleSigninComponent } from './login/google-signin/google-signin.component';

@NgModule(
  {
    declarations: [
      LoginComponent,
      RegistrationComponent,
      AuthComponent,
      ResetPasswordComponent,
      GoogleSigninComponent
    ],
    imports: [
      CommonModule,
      AuthRoutingModule,
      SharedModule
    ]
  }
)
export class AuthModule {}
