import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Message} from "../../shared/models/message.model";
import {AuthService} from "../../shared/services/auth.service";

@Component({
  selector: 'book-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  message: Message;

  constructor(private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute,
             /* private authservice: AuthService */) {
  }

  ngOnInit() {
    console.log(window.localStorage.getItem('token'));
    console.log(this.authService.isLoggedIn());
    this.message = new Message('warning', '');

    this.route.queryParams.subscribe( (params: Params) => {
          if (params['nowCanLogin']) {
            this.showMessage({
              type: 'success',
              text: 'Now, you can enter to BooksApp'
            })
          }
          else if(params['accessDenied']){
            this.showMessage({
              type: 'warning',
              text: 'Log-in to enter the app'
            })
          }
          else if (params['resetPassword']) {
            this.showMessage({
              type: 'success',
              text: 'Your password was updated. Now, you can enter to BooksApp'
            })
          }
          else if (params['code']) {
            console.log(window.localStorage.getItem('token'));
            console.log('google');
            console.log(params['code']);

            this.authService.googleAuthGetToken(params['code'], window.localStorage.getItem('token')).subscribe( (response: any) => {
              console.log(response);

              if (response.token){
                this.message.text = '';
                this.authService.loginSuccess(JSON.stringify(response));
                this.router.navigate(['/system', 'books']);
              }
              else{
                this.showMessage({
                  type: 'danger',
                  text: 'Something went wrong... Register to enter the BooksApp'
                })
              }

            });

          }
      });

    this.form = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(6)])
    })
  }

  private showMessage(message: Message){
    this.message = message;

    window.setTimeout(() => this.message.text = '', 5000);
  }

  onSubmit(){
    const formData = this.form.value;

    this.authService.login(formData)
      .subscribe((response: any) => {
        if (response === "Login isnt valid" || response === "Password is not correct"){
          this.showMessage({
            type: 'danger',
            text: 'Login or password is not correct'
          })
        }
        /*
        if (response === "Login isnt valid"){
          this.showMessage({
            type: 'danger',
            text: 'Such user doesn\'t exists'
          })
        }
        else if (response === "Password is not correct"){
          this.showMessage({
            type: 'danger',
            text: 'Password is incorrect'
          })
        }
        */
        else if (JSON.parse(response).token){

          this.message.text = '';
          this.authService.loginSuccess(response);
          this.router.navigate(['/system', 'chat']);
        }

      });
  }
}
