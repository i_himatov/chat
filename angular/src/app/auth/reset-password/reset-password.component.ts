import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Message} from "../../shared/models/message.model";
import {AuthService} from "../../shared/services/auth.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {UserService} from "../../shared/services/user.service";
import {User} from "../../shared/models/user.model";

@Component({
  selector: 'book-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.less']
})
export class ResetPasswordComponent implements OnInit {
  form: FormGroup;
  message: Message;
  step: number = 1;
  currentUser: User;
  hide: boolean = false;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private authService: AuthService,
              private userService: UserService) {
  }

  ngOnInit() {
    console.log(window.localStorage.getItem('token'));
    console.log(this.authService.isLoggedIn());
    this.message = new Message('warning', '');

    this.route.queryParams.subscribe( (params: Params) => {
      if (params['token']) {
        console.log(params['token']);
        this.step = 0;
        this.userService.getUserByResetToken(params['token']).subscribe((response: any) => {
          console.log(response);
          if (response === "Not found"){
            this.router.navigate(['/login']);
          }
          else if (JSON.parse(response)._id) {
            this.step = 2;
            this.form = new FormGroup({
              'password1': new FormControl(null, [Validators.required, Validators.minLength(6)]),
              'password2': new FormControl(null, [Validators.required, Validators.minLength(6)])
            });
            this.currentUser = JSON.parse(response);
            console.log(this.currentUser);

          }
          else {
            console.log(response);
            this.step = 4;
            this.showMessage({
              type: 'warning',
              text: 'Something went wrong... Please try again later'
            })
          }
        })
      }

    });

    if (this.step === 1){
      this.form = new FormGroup({
        'email': new FormControl(null, [Validators.required, Validators.email], this.validEmails.bind(this))
      })
    }


  }

  private showMessage(message: Message){
    this.message = message;

   /* window.setTimeout(() => this.message.text = '', 5000); */
  }

  onSubmit(){
    const formData = this.form.value;
    console.dir(formData);

    if (this.step === 1) {
      this.userService.createUserResetToken(formData.email)
        .subscribe((response: any) => {
          console.log(response);
          if (response === "Not found") {
            this.showMessage({
              type: 'danger',
              text: 'User with such e-mail doesn\'t exist'
            })
          }
          else if (response === "Email Sent") {
            this.step = 5;
            this.hide = true;
            this.showMessage({
              type: 'success',
              text: 'The letter was send to your e-mail. Follow the link to reset your password.'
            })
          }
        });
    }
    else if(this.step === 2) {
      // params['token']
      console.log(this.currentUser._id);
      this.userService.updatePassword(this.currentUser._id, formData)
        .subscribe((response: any) => {
          if (response !== ''){
            this.router.navigate(['/login'], {
              queryParams: {
                resetPassword: true
              }
            });
          }
        });
    }

    }

  validEmails (control: FormControl): Promise<any> {
    return new Promise((resolve, reject) => {
      this.userService.getUserByEmail(control.value)
        .subscribe((response: any) => {
          console.dir(response);
          if (response === "Not found") {
            resolve({validEmail: false});
          }
          else {
            resolve(null);
          }

        })
    });
  }

}
