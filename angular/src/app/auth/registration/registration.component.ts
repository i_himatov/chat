import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {User} from "../../shared/models/user.model";
import {UserService} from "../../shared/services/user.service";
import {Promise, reject, resolve} from "q";
import {ProcessFunction} from "jasmine-spec-reporter/built/display/logger";

@Component({
  selector: 'book-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.less']
})
export class RegistrationComponent implements OnInit {
  form: FormGroup;
  constructor(private router: Router,
              private userService: UserService) { }

  ngOnInit() {
    this.form = new FormGroup({
      'userName': new FormControl(null, [Validators.required]),
      'email': new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails.bind(this)),
      'password': new FormControl(null, [Validators.required, Validators.minLength(6)])
    });
  }

  onSubmit(){
    const {userName, email, password} = this.form.value;
    const user = new User(userName, email, password);
    this.userService.createNewUser(user).subscribe(() => {
      this.router.navigate(['/login'], {
        queryParams: {
          nowCanLogin: true
        }
      });
    });
  }

  forbiddenEmails (control: FormControl): Promise<any> {
    return Promise((resolve, reject) => {
      this.userService.getUserByEmail(control.value)
        .subscribe((response: any) => {
        /* if (response._body === "Not found") */
          if (response === "Not found") {
            resolve(null);
          }
          else {
            resolve({forbiddenEmail: true});
          }
        })
    });
  }
}
