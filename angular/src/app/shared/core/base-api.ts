import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class BaseApi{
  private baseUrl = 'http://localhost:3000';
  constructor(public http: HttpClient){
  }

  private getUrl(url: string = ''): string{
    return this.baseUrl + url;
  }

  public get(url: string = '', options: any = {}): Observable<any> {
    return this.http.get(this.getUrl(url), options);
  }

  public post(url: string = '', data: any = {}, options: any = {}): Observable<any> {
    return this.http.post(this.getUrl(url), data, options);
  }

  public put(url:string = '', data: any = {}, options: any = {}): Observable<any> {
    return this.http.put(this.getUrl(url), data, options);
  }

  public delete(url:string = '', options: any = {}): Observable<any> {
    return this.http.delete(this.getUrl(url), options);
  }
}
