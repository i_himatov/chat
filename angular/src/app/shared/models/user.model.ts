export class User{
  constructor(
    public userName: string,
    public email: string,
    public password: string,
    public _id?: any,
    public socketId?: string
  ){}
}
