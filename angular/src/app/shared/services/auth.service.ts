import {Injectable} from "@angular/core";
import {BaseApi} from "../core/base-api";
import {Observable} from "rxjs/Observable";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";

@Injectable()
export class AuthService extends BaseApi{
  constructor(public http: HttpClient){
    super(http);
  }

  login(formData: any): Observable<any>{
    return this.post('/auth/login', formData, {responseType: 'text'});
  }


  loginSuccess(response: any){
    window.localStorage.setItem('token', JSON.stringify(JSON.parse(response).token));
    window.localStorage.setItem('email', JSON.stringify(JSON.parse(response).email));
    window.localStorage.setItem('userId', JSON.stringify(JSON.parse(response).userId));
  }

  logout(){
    window.localStorage.clear();
  }

  isLoggedIn(): boolean {
    return window.localStorage.getItem('token') !== null;
  }

  googleAuthGetCode(): Observable<any>{
    var x =  this.get(`/user/google`, {responseType: 'text'});
    console.log(x);
    return x;
  }
  googleAuthGetToken(code: string, token: string){
    console.log('googleAuthGetToken');
    console.log('/user/google/gettoken/' + code);
    var headers = new HttpHeaders({ 'Authorization': 'Bearer ' + token});
    let params = new HttpParams();
    params = params.append('code', code);
    return this.get('/user/google/gettoken/', { headers: headers, params: params});
  }
}
