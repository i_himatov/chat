import {BaseApi} from "../core/base-api";
import {Observable} from "rxjs/Observable";
import {User} from "../models/user.model";
import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class UserService extends BaseApi{
  constructor(public http: HttpClient){
    super(http);
  }

  getUserByEmail(email: string): Observable<User>{
    return this.get(`/user/email/${email}`, {responseType: 'text'});
  }

  getUserByResetToken(resetToken: string): Observable<User>{
    return this.get(`/user/token/${resetToken}`, {responseType: 'text'});
  }

  createUserResetToken(email: string): Observable<User>{
    return this.get(`/user/resetpassword/${email}`, {responseType: 'text'});
  }

  updatePassword (id: any, password: any): Observable<any>{
    return this.put('/user/resetpassword/' + id, password, { responseType: 'text' });
  }

  createNewUser(user: User): Observable<User> {
    return this.post('/user', user);
  }

  getAllUsers(): any{
    var headers = new HttpHeaders({
      'Authorization': 'Bearer ' + JSON.parse(window.localStorage.getItem('token'))
    });
    return this.get('/user/', {headers: headers});
  }
}
