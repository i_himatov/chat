import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Book} from "../shared/models/book.model";
import {BooksService} from "../shared/services/books.service";
import {Message} from "../../shared/models/message.model";

@Component({
  selector: 'app-add-book-page',
  templateUrl: './add-book-page.component.html',
  styleUrls: ['./add-book-page.component.less']
})
export class AddBookPageComponent implements OnInit {
  form: FormGroup;
  fileAttached: Boolean = false;
  fileAttachedName: string = '';
  @ViewChild('file') InputFile: ElementRef;
  @ViewChild('inputWarning') inputWarning: ElementRef;
  message: Message;
  bookImage: any;
  saveBook: boolean = false;

  constructor(private booksService: BooksService) { }

  ngOnInit() {
    this.message = new Message('warning', '');
    this.form = new FormGroup({
      'author': new FormControl(null, [Validators.required]),
      'title': new FormControl(null, [Validators.required]),
      'description': new FormControl('Description', [Validators.required]),
      'status': new FormControl(null, [Validators.required])
    });

    window.localStorage.setItem('currentSignId', JSON.stringify(this.makeId()));
    console.log(JSON.parse(window.localStorage.getItem('currentSignId')));
  }

  makeId(): string {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 25; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  onImageChangeFromFile(event){
    console.log(event);
    console.dir(event.target);
    if (event.target.files && event.target.files[0]){
      this.fileAttached = true;
      this.fileAttachedName = event.target.files[0].name;
      this.bookImage = event.target.files[0];
    }

  }

  prepareSave(){
    /*
    let upload = this.InputFile.nativeElement.files[0];
    console.log(upload);
    console.dir(upload);
    */
    const {author, title, description, status} = this.form.value;
    let input = new FormData();
    input.append('author', this.form.get('author').value);
    input.append('title', this.form.get('title').value);
    input.append('description', this.form.get('description').value);
    input.append('status', this.form.get('status').value);
    input.append('bookImage', '-');
    input.append('currentSignId', JSON.parse(window.localStorage.getItem('currentSignId')));
    return input;
  }


  private showMessage(message: Message){
    this.message = message;

    window.setTimeout(() => {this.message.text = '';}, 5000);
  }

  reset(){
    /*
    console.log(this.InputFile.nativeElement.files);
    this.InputFile.nativeElement.value = "";
    console.log(this.InputFile.nativeElement.files);
    this.inputWarning.nativeElement.innerHTML = "";
    this.fileAttached = false;
    */
    this.form = new FormGroup({
      'author': new FormControl(null, [Validators.required]),
      'title': new FormControl(null, [Validators.required]),
      'description': new FormControl("Description", [Validators.required]),
      'status': new FormControl(null, [Validators.required])
    });
  }

  resetAttachments(){
    this.saveBook = !this.saveBook;
  }

  onSubmit(){
    const formModel = this.prepareSave();
    this.booksService.createNewBook(formModel).subscribe(() => {
      this.reset();
      this.showMessage({
        type: 'success',
        text: 'New Book was successfully added'
      });
      window.localStorage.removeItem("currentSignId");
      window.localStorage.setItem('currentSignId', JSON.stringify(this.makeId()));
      console.log(JSON.parse(window.localStorage.getItem('currentSignId')));
    })
  }
}
