import {BaseApi} from "../../../shared/core/base-api";
import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Message} from "../models/message.model";

@Injectable()
export class ChatService extends BaseApi {
  constructor(public http: HttpClient){
    super(http);
  }

  getChat(currentUserId: String, userPairId: String): Observable<any>{
    var headers = new HttpHeaders({
      'Authorization': 'Bearer ' + JSON.parse(window.localStorage.getItem('token')),
    });
    let params = new HttpParams();
    params = params.append('user1', String(currentUserId));
    params = params.append('user2', String(userPairId));
    return this.get('/chats', {headers: headers, params: params /*, responseType: 'text'*/});
  }

  sendMessage(message:any): Observable<Message> {
    var headers = new HttpHeaders({ 'Authorization': 'Bearer ' + JSON.parse(window.localStorage.getItem('token'))});
    return this.post('/chats', message, { headers: headers });
  }

}
