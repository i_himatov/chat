import {BaseApi} from "../../../shared/core/base-api";
import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Book} from "../models/book.model";

@Injectable()
export class BooksService extends BaseApi {
  constructor(public http: HttpClient){
    super(http);
  }

  getBooksNew(currentControl: number, searchString: string, sort: object): Observable<any[]> {
      var headers = new HttpHeaders({
        'Authorization': 'Bearer ' + JSON.parse(window.localStorage.getItem('token')),
        // 'Content-Type': 'application/x-www-form-urlencoded'
      });
      let params = new HttpParams();
      params = params.append('currentcontrol', String(currentControl));
      params = params.append('sort', JSON.stringify(sort));
      params = params.append('search', String(searchString));
      console.log(params, String(currentControl),JSON.stringify(sort), String(searchString) )
      return this.get('/books', {headers: headers, params: params});
  }

  getCountOfBooks(searchString: string): Observable<any[]>{
      var headers = new HttpHeaders({
        'Authorization': 'Bearer ' + JSON.parse(window.localStorage.getItem('token')),
      //  'Content-Type': 'application/x-www-form-urlencoded'
      });
      let params = new HttpParams();
      params = params.append('search', String(searchString));
      return this.get('/books/count', {headers: headers, params: params});

     // return this.get('/books/count?' + 'search=' + String(searchString), {headers: headers});

  }

  createNewBook(book:any): Observable<Book> {
    var headers = new HttpHeaders({ 'Authorization': 'Bearer ' + JSON.parse(window.localStorage.getItem('token'))});
    return this.post('/books', book, { headers: headers });
  }

  getBookById(id: any): Observable<Book> {
    var headers = new HttpHeaders({ 'Authorization': 'Bearer ' + JSON.parse(window.localStorage.getItem('token'))});
    return this.get('/books/' + id, { headers: headers });
  }

  editBook(id: any, book: any, token: any): Observable<any>{
    var headers = new HttpHeaders({ 'Authorization': 'Bearer ' + token});
    return this.put('/books/' + id, book, { headers: headers });
  }

  deleteBook(id: any): Observable<any>{
    var headers = new HttpHeaders(
        {'Authorization': 'Bearer ' + JSON.parse(window.localStorage.getItem('token'))}
      );

    let params = new HttpParams();
    params = params.append('id', String(id));
    console.log(params, String(id) );
    return this.delete('/books/delete',{ headers: headers, responseType: 'text', params: params});
  }

}
