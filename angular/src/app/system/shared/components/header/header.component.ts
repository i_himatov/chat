import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../../shared/services/auth.service";
import {Router} from "@angular/router";
import {User} from "../../../../shared/models/user.model";

@Component({
  selector: 'chat-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  email: string;

  constructor(private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    this.email = JSON.parse(JSON.stringify(localStorage.getItem('email')));
  }

  onLogout(){
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}
