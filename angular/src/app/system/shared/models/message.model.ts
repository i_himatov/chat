export class Message{
  constructor(
    public from: string,
    public to: string,
    public text: string,
    public createdAt?: string,
  ){}
}
