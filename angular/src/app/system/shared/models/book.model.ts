export class Book{
  constructor(
    public author: string,
    public title: string,
    public description: string,
    public status: string,
    public bookRating?: any,
    public _id?: any,
    public bookImageName?: any,
    public currentSignId?: any
  ){}
}
