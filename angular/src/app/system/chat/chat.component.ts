import {AfterContentChecked, AfterViewChecked, Component, OnChanges, OnInit} from '@angular/core';
import {UserService} from "../../shared/services/user.service";
import {User} from "../../shared/models/user.model";
import {NgForm} from "@angular/forms";
import {ChatService} from "../shared/services/chat.service";
import * as io from "socket.io-client";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.less']
})
export class ChatComponent implements OnInit {
  users = [];
  currentUser: User;
  showChat: boolean = false;
  currentUserID: String;
  userPairID: String;
  // userPairSocketID: String;
  noMessages: boolean = true;
  msgData = {};
  socket = io('http://localhost:4000');
  chatHistory = [];

  constructor(private userService: UserService,
              private chatService: ChatService) {}

  ngOnInit() {
    this.currentUserID = JSON.parse(window.localStorage.getItem('userId'));
    console.log(this.currentUserID);
    // console.log(typeof this.currentUserID);
    this.userService.getAllUsers().subscribe((response: User[]) => {
      console.log(response);
      let allUsers = response;
      this.users = allUsers.filter((user) => {
        return user._id !== this.currentUserID;
      });
    });
    this.socket.on('start', () => {
      console.log('start');
      console.log(this.currentUserID);
      this.socket.emit('add-socket-id', this.currentUserID);
    });

    this.socket.on('new-message', function (data) {
      console.log(data);
      console.log('zzzzzz');
      this.chatHistory.push(data.message);
      this.noMessages = false;
      this.msgData = {};
      console.log(this.chatHistory);

    }.bind(this));
  }

  getChat(user: User){
    this.currentUser = user;
    this.chatHistory = [];
    this.userPairID = user._id;
    // this.userPairSocketID = user.socketId;
    console.log(this.userPairID);

    this.chatService.getChat(this.currentUserID, this.userPairID).subscribe((response) => {
        this.showChat = true;
        this.chatHistory = response;

        if (this.chatHistory.length > 0)
          this.noMessages = false;
        else
          this.noMessages = true;
    })

  }

  sendMessage(form: NgForm) {
    /*
    console.dir(form);
    console.log(form.value.message);
    */

    this.msgData = {
      from: this.currentUserID,
      to: this.userPairID,
      text: form.value.message
    };
    console.log(this.msgData);

    this.chatService.sendMessage(this.msgData).subscribe((response) => {
      this.noMessages = false;
      console.log('+++');
      console.log(this.chatHistory);
      console.log('+++');
      console.log(response);
      this.msgData = {};
      this.socket.emit('save-message', response /*, this.userPairSocketID */);
    });

  }
}
