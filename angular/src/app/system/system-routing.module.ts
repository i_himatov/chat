import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

import {SystemComponent} from "./system.component";
import {BooksPageComponent} from "./books-page/books-page.component";
import {AddBookPageComponent} from "./add-book-page/add-book-page.component";
import {EditBookPageComponent} from "./edit-book-page/edit-book-page.component";
import {AuthGuard} from "../shared/services/auth.guard";
import {ChatComponent} from "./chat/chat.component";

const routes: Routes = [
  {path: 'system', component: SystemComponent, canActivate: [AuthGuard], children: [
    {path: 'chat', component: ChatComponent}]}

  /*
  SystemComponent, canActivate: [AuthGuard], children: [
    {path: 'books', component: BooksPageComponent},
    {path: 'add', component: AddBookPageComponent},
    {path: 'edit', component: EditBookPageComponent}
  ]} */
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemRoutingModule{}
