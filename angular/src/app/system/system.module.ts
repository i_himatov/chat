import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import {SystemRoutingModule} from "./system-routing.module";
import {SystemComponent} from "./system.component";
import {BooksPageComponent} from "./books-page/books-page.component";
import {AddBookPageComponent} from "./add-book-page/add-book-page.component";
import {EditBookPageComponent} from "./edit-book-page/edit-book-page.component";
import { HeaderComponent } from './shared/components/header/header.component';
import { BooksListComponent } from './books-page/books-list/books-list.component';
import {BooksService} from "./shared/services/books.service";
import { BookDetailsComponent } from './books-page/book-details/book-details.component';

import { NgUploaderModule } from 'ngx-uploader';
import { FilesUploaderComponent } from './add-book-page/files-uploader/files-uploader.component';
import { ChatComponent } from './chat/chat.component';
import {ChatService} from "./shared/services/chat.service";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SystemRoutingModule,
    NgUploaderModule
  ],
  declarations: [
    SystemComponent,
    BooksPageComponent,
    AddBookPageComponent,
    EditBookPageComponent,
    HeaderComponent,
    BooksListComponent,
    BookDetailsComponent,
    FilesUploaderComponent,
    ChatComponent
  ],
  providers: [
    BooksService,
    ChatService
  ]
})
export class SystemModule{}
