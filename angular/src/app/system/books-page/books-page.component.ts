import { Component } from '@angular/core';
import {Book} from "../shared/models/book.model";

@Component({
  selector: 'app-books-page',
  templateUrl: './books-page.component.html',
  styleUrls: ['./books-page.component.less']
})
export class BooksPageComponent  {
  chosenBook: Book;
  chosenBookId: any;
  doubleClick: boolean = false;
  constructor() { }


  setChosenBook(book: Book){
    this.chosenBook = book;
    if (!this.chosenBookId)
      this.chosenBookId = this.chosenBook._id;
    else if (this.chosenBookId === this.chosenBook._id)
      this.doubleClick = !this.doubleClick;
    else if (this.chosenBookId !== this.chosenBook._id){
      this.chosenBookId = this.chosenBook._id;
      this.doubleClick = false;
    }
  }

}
