import {Component, ElementRef, Input, OnChanges, Renderer2, ViewChild} from '@angular/core';

@Component({
  selector: 'book-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.less']
})
export class BookDetailsComponent implements OnChanges{
  @Input() chosenBook;
  @ViewChild('image') image: ElementRef;
  constructor(private renderer: Renderer2) { }

  ngOnChanges() {
    this.renderer.removeAttribute(this.image.nativeElement, 'src');
    this.renderer.setAttribute(this.image.nativeElement, 'src', 'http://localhost:3000/books/images/'
      + this.chosenBook.bookImageName + '/' + JSON.parse(window.localStorage.getItem('token')));
  }

}
