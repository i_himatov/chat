import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Book} from "../../shared/models/book.model";
import {Router} from "@angular/router";
import {BooksService} from "../../shared/services/books.service";
import {Message} from "../../../shared/models/message.model";

@Component({
  selector: 'book-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.less']
})
export class BooksListComponent implements OnInit {
  books: any;
  @Output() onChosenBook = new EventEmitter<Book>();
  deletedBook: any = '';
  booksLength: number;
  numberOfBooks: Array<number> = [];
  currentNumberOfBooksMap: Array<number> = [];
  currentNumberOfBooks: Array<number> = [];
  controlsArray: Array<number> = [];
  searchValue = '';
  searchView: boolean = false;
  currentControl: number = 1;
  message: Message;
  isLoaded = false;
  sortFlag: boolean = false;
  modalDialog: boolean = false;
  sort: object = {};

  constructor(private router: Router,
              private bookService: BooksService) { }

  ngOnInit() {
    this.getBooksNew();
  }

  getBooksNew(){
    console.log(this.searchValue + '====');
    this.bookService.getBooksNew(this.currentControl, this.searchValue, this.sort).subscribe((response: any) => {
      this.books = response;
      console.log(this.books);
      this.isLoaded = true;
      this.message = new Message('warning', '');

      this.bookService.getCountOfBooks(this.searchValue).subscribe((response: any) => {
        this.booksLength = response.count;
        if (this.books.length === 0){
          this.currentControl = this.currentControl - 1;
          this.getBooksNew();
        }
        else {
          this.setControls();
          this.getNumberOfBooks();
          this.setCurrentNumberOfBooks();
        }

      });
    });

  }

  getNumberOfBooks(){
    let numberOfBooks = [];
    console.log(this.booksLength);
    for (let i = 1; i <= this.booksLength; i++){
      numberOfBooks.push(i);
    }
    this.numberOfBooks = numberOfBooks;
    console.log(this.numberOfBooks);
  }

  setCurrentNumberOfBooks(){
    this.currentNumberOfBooksMap[1] = 3 * this.currentControl;
    this.currentNumberOfBooksMap[0] = this.currentNumberOfBooksMap[1] - 3;
    this.currentNumberOfBooks = this.numberOfBooks.slice(this.currentNumberOfBooksMap[0], this.currentNumberOfBooksMap[1]);
  }

  private showMessage(message: Message){
    this.message = message;

    window.setTimeout(() => {
      this.message.text = '';
    }, 5000);
  }

  setControls(){
     let controlsCount = Math.ceil(this.booksLength / 3);
     let controlsArray = [];
     for (let i = 1; i <= controlsCount; i++){
       controlsArray.push(i);
     }
     this.controlsArray = controlsArray;
  }

  setMySort(attr) {
    if (typeof this.sort[attr] === "undefined") {
      this.sort = {};
      this.sort[attr] = 1;
    } else {
      this.sort[attr] *= -1;
    }
    this.bookService.getBooksNew(this.currentControl, this.searchValue, this.sort).subscribe((response: any) => {
      this.books = response;
      this.bookService.getCountOfBooks(this.searchValue).subscribe((response: any) => {
        this.booksLength = response.count;
        console.log(this.booksLength);
        this.setControls();
        this.getNumberOfBooks();
        this.setCurrentNumberOfBooks();
      });
    });
    console.log(this.sort);

  }

  Update(control){
    this.currentControl = control;
     this.getBooksNew();
  }

  changeView(){
    if (this.sortFlag)
      this.sortFlag = !this.sortFlag;
    if (this.searchValue != '') {
      this.searchView = true;
    }
    else if (this.searchValue == '')
    {
      this.searchView = false;
    }
    this.currentControl = 1;
    this.getBooksNew();
  }

  setChosenBook(book: Book){
    this.onChosenBook.emit(book);
  }

  onEdit(id: any){
    this.router.navigate(['/system','edit'], {
      queryParams: {
        bookId: id
      }
    })
  }

  onModal(book: Book){
    this.deletedBook = book;
    this.modalDialog = true;
  }

  onDelete(){
    this.bookService.deleteBook(this.deletedBook._id).subscribe(() => {
      this.modalDialog = false;
      this.showMessage({
          type: 'success',
          text: 'The Book was successfully deleted'
        }
      );
      this.getBooksNew();
    });
  }

  onCancelDelete(){
    this.deletedBook = '';
    this.modalDialog = false;
  }

}
