import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Book} from "../shared/models/book.model";
import {BooksService} from "../shared/services/books.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Message} from "../../shared/models/message.model";

@Component({
  selector: 'app-edit-book-page',
  templateUrl: './edit-book-page.component.html',
  styleUrls: ['./edit-book-page.component.less']
})
export class EditBookPageComponent implements OnInit {
  form: FormGroup;
  fileAttached: Boolean = false;
  fileAttachedName: string = '';
  @ViewChild('file') InputFile: any;
  @ViewChild('inputWarning') inputWarning: any;
  message: Message;
  bookId: any;
  book: Book;
  isLoaded = false;
  Permission = true;
  bookImage: any = '';

  constructor(private booksService: BooksService,
              private router: Router,
              private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.message = new Message('warning', '');
    this.route.queryParams.subscribe( (params: Params) => {
      if (params['bookId']) {
        this.bookId = params['bookId'];
        this.booksService.getBookById(this.bookId).subscribe((book: Book) => {
          this.book = book;
          this.isLoaded = true;
          this.form = new FormGroup({
            'author': new FormControl(this.book.author, [Validators.required]),
            'title': new FormControl(this.book.title, [Validators.required]),
            'description': new FormControl(this.book.description, [Validators.required]),
            'status': new FormControl(this.book.status.toUpperCase(), [Validators.required])
          });
        });
      }
    });
  }

  onImageChangeFromFile(event){
    console.log(event);
    if (event.target.files && event.target.files[0]){
      this.fileAttached = true;
      this.fileAttachedName = event.target.files[0].name;
      this.bookImage = event.target.files[0];
    }

  }

  private showMessage(message: Message/*, callback: any*/){
    this.message = message;
    window.setTimeout(() => {
      this.message.text = '';
      this.Permission = true;
      /* callback(); */
    }, 5000);

  }

  prepareSave(){
    let upload = this.InputFile.nativeElement.files[0];
    console.log(upload);
    console.dir(upload);
    const {author, title, description, status} = this.form.value;
    let input = new FormData();
    input.append('author', this.form.get('author').value);
    input.append('title', this.form.get('title').value);
    input.append('description', this.form.get('description').value);
    input.append('status', this.form.get('status').value);
    input.append('bookImage', this.bookImage);
    console.log(typeof (this.bookImage));
    return input;
  }

  onSubmit() {
    const formModel = this.prepareSave();
    console.dir(formModel);
    console.log(formModel['author']);
    let token = JSON.parse(window.localStorage.getItem('token'));
    this.booksService.editBook(this.bookId, formModel, token).subscribe(() => {
      this.Permission = false;
      this.showMessage({
        type: 'success',
        text: 'The Book was successfully edited'
      });
      /*,
             () => this.router.navigate(['/system', 'books'])
           );
     */
    });
  }

}
