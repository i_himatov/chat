const nodemailer = require('nodemailer');
var ejs = require('ejs');
var fs = require('fs');

module.exports = function (emailTo, subject, templateEmail, templateData, cb) {
    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: 'igor.gimatov881@gmail.com',
            pass: '12344321a'
        }

    });

    var file = fs.readFileSync(__dirname + '/../views/' +  templateEmail + '.ejs', 'ascii');
    var template = ejs.render(file, templateData);

    var mailOptions = {
        from: 'igor.gimatov881@gmail.com',
        to: emailTo,
        subject: subject,
        html: template
    };

    transporter.sendMail(mailOptions, function (err, result) {
        console.log(mailOptions);
        cb(err);
    })

};