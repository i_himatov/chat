var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./config/dev');
var mongoose = require('mongoose');
var jwt = require('jwt-express');

var cron = require('node-cron');
var fs = require('fs');

/*
var googleAuth = require('./services/googleService');
// retrieve an access token]
googleAuth();
*/

mongoose.Promise = global.Promise;
// console.log(config.db)

mongoose.connect(config.db, { useMongoClient: true });

require("./models");

var index = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(jwt.init(config.secret, {
    cookies: false
}));

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '/public')));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

require(__dirname + "/routes")(app);
// app.use('/', index);
// app.use('/user', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Clean TempFile every 1 hour
var task = cron.schedule('0 0 */1 * * *', function() {
  /*  console.log('immediately started'); */

    var TempFile = mongoose.model("TempFile");
    var time = +new Date - 60 * 60 * 1000;

    var query = TempFile.find({time: { $lte:  time}});
    query.exec(function (err, tempfiles) {
        if (err) {
            return console.log(err);
        }
        console.log(tempfiles);
        var arr = [];
        for (i = 0; i < tempfiles.length; i++){
            arr.push(tempfiles[i].finalFileName);
        }
        console.log(arr);


        TempFile.remove({time: { $lte:  time}}, function (err) {
            if (err) {
                console.log(err);
            }
            console.log("DELETED trash in TempFile");

            arr.forEach(function(filepath){
                fs.unlink(__dirname + '/public/images/' + filepath, function(error) {
                    if (error) {
                        throw error;
                    }
                    console.log('Image deleted' + filepath);
                });
            });
        });

    });

}, false);

task.start();

module.exports = app;
