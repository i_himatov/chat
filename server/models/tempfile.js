var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var TempFileSchema = new Schema({
    currentSignId: {
        type: String
    },
    fileName: {
        type: String
    },
    finalFileName: {
        type: String
    },
    size: {
        type: Number
    },
    status: {
        type: String
    },
    type: {
        type: String
    },
    path: {
        type: String
    },
    time: {
        type: String
    }
}, {
    toJSON: {
        transform: function (doc, ret) {
            delete ret.__v;
        }
    },
    toObject: {
        transform: function (doc, ret) {
            delete ret.__v;
        },
        virtuals: true,
    },
});

module.exports = mongoose.model('TempFile', TempFileSchema);

