var mongoose = require("mongoose");
var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;

var BookSchema = new Schema({
    author: {
        type: String /*,
        required: [true, "This field is required"] */
    },
    title: {
        type: String /*,
        required: [true, "This field is required"] */
    },
    description: {
        type: String /*,
        required: [true, "This field is required"] */
    },
    status: {
        type: String /*,
        required: [true, "This field is required"] */
    },
    bookImageName: {
        type: String /*,
        required: [true, "This field is required"] */
    },
    attachments: {
        type: Object
    },
    bookRating: {
        type: Number
    }
}, {
    toJSON: {
        transform: function (doc, ret) {
            delete ret.__v;
        }
    },
    toObject: {
        transform: function (doc, ret) {
            delete ret.__v;
        },
        virtuals: true,
    },
});

module.exports = mongoose.model('Book', BookSchema);

