var mongoose = require("mongoose");
var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;
var sendMail = require('../services/sendmail');

var UserSchema = new Schema({
    userName: {
        type: String,
        required: [true, "This field is required"],
    },
    email: String,
    /*
    age: {
        type: Number,
        min: 0
    },
    */
    password: {
        type: String,
        required: [true, "This field is required"],
        minlength: [6, "Min length is 6 symbols"],
    },
    resetToken: {
        type: String
    },
    socketId: {
        type: Array
    }
}, {
    toJSON: {
        transform: function (doc, ret) {
            delete ret.__v;
            delete ret.password;
        }
    },
    toObject: {
        transform: function (doc, ret) {
            delete ret.__v;
            delete ret.password;
        },
        virtuals: true,
    },
});

UserSchema.pre("save", function (next) {
    var user = this;
    if (user.isModified('password') || user.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) return next(err);
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) return next(err);
                user.password = hash;
                next();
            });
        });
    } else
        next();
});

UserSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

UserSchema.methods.sendResetPasswordEmail = function (jwtGenerator, cb) {
    var self = this;
    var token =  jwtGenerator({
        id: self._id
    }).token;

    self.constructor.update({_id: self._id}, {$set: {resetToken : token}}, function (err, updatedResult) {
        if (err) {
            return cb(err);
        }
        /* res.status(200).send(updatedResult); */
        // sendmail()
        var templateEmail = 'letter';
        sendMail(self.email, "Restore password link", templateEmail, {token: token}, cb);
    });
};

UserSchema.methods.sendPassword = function(password) {
    var self = this;
    var templateEmail = 'passwordLetter';
    sendMail(self.email, "Registration on BooksApp has been successfully completed", templateEmail, {password: password}, function(err) {
        if(err){
            console.log('Error');
            console.log(err);
            res.status(500).send(err);
        } else {
            console.log('Email with password sent');
            res.status(200).send('Email sent');
        }
    });
};

module.exports = mongoose.model('User', UserSchema);

