var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MessagesSchema = new Schema({
    from: String,
    to: String,
    text: String,
    created_at: { type: Date, default: Date.now },
}, {
    toJSON: {
        transform: function (doc, ret) {
            delete ret.__v;
        }
    },
    toObject: {
        transform: function (doc, ret) {
            delete ret.__v;
        },
        virtuals: true,
    },
});

module.exports = mongoose.model('Messages', MessagesSchema);