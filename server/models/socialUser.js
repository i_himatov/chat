var mongoose = require("mongoose");
// var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;

var SocialUserSchema = new Schema({
    service: {
        type: String,
        required: [true, "This field is required"]
    },
    token: {
        type: String,
        required: [true, "This field is required"]
    },
    refresh_token: {
        type: String,
        required: [true, "This field is required"]
    },
    user_id: {
        type: String,
        required: [true, "This field is required"]
    },
    email: {
        type: String,
        required: [true, "This field is required"]
    },
    name: {
        type: String,
        required: [true, "This field is required"]
    },
    socialId: {
        type: String,
        required: [true, "This field is required"]
    },



}, {
    toJSON: {
        transform: function (doc, ret) {
            delete ret.__v;
            delete ret.token;
        }
    },
    toObject: {
        transform: function (doc, ret) {
            delete ret.__v;
            delete ret.token;
        },
        virtuals: true,
    },
});

module.exports = mongoose.model('SocialUser', SocialUserSchema);