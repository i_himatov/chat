var mongoose = require('mongoose');

module.exports = function (tokens, user, socialId, cb, cbError) {
    var SocialUser = mongoose.model("SocialUser");
    var newSocialUser = {
        service: 'google',
        token: tokens.access_token,
        refresh_token: tokens.refresh_token,
        user_id: user._id,
        email: user.email,
        name: user.userName,
        socialId: socialId /* response.data.id */
    }
    console.log(newSocialUser);
    SocialUser.create(newSocialUser, function (err, createdSocialUser) {
        if (err) {
            var code = err.name === "ValidationError" ? 400 : 500;
            cbError(err, code);
            /*
            return res.status(code).send(err);
            */
        }
        else{
            console.log(createdSocialUser);
            console.log('+++');
            console.log(createdSocialUser.user_id);
            cb(createdSocialUser);
        }

    });
}