var mongoose = require('mongoose');

module.exports = function (userName, email, cb) {
    var generatePassword = function(){
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 15; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }
    var password = generatePassword();

    var User = mongoose.model("User");
    var newUser = {
        userName: userName, /* response.data.displayName, */
        email: email, /* response.data.emails[0].value, */
        password: password
    }
    User.create(newUser, function (err, createdUser) {
        if (err) {
            var code = err.name === "ValidationError" ? 400 : 500;
            return res.status(code).send(err);
        }
        else {
            cb(createdUser, password);
        }

    });
};