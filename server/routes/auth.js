var express = require('express');
var router = express.Router();
var mongoose = require("mongoose");

module.exports = function (app) {
    app.use("/auth", router);
    router.post('/login', function(req, res) {
        var User = mongoose.model("User");
        User.findOne({email: req.body.email}).exec(function (err, user) {

            if (err) {
                /* res.status(500); */
                return res.send(err);
            }
            if (!user) {
                /* res.status(403); */
                return res.send("Login isnt valid");
            }
            user.comparePassword(req.body.password || "", function (err, isMatch) {
                if (err) {
                    /* res.status(500); */
                    return res.send(err);
                }
                if (!isMatch) {
                   /*  res.status(403); */
                    return res.send("Password is not correct");
                }
                res.status(200).send({
                    token:  res.jwt({
                            id: user.id
                            }).token,
                    email: user.email,
                    userId: user._id
                });
            });
        })
    });
}