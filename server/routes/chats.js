var express = require('express');
var router = express.Router();
var mongoose = require("mongoose");
var isLoggined = require(__dirname + "/../middlewares/isLoggined");
var currentUser = require(__dirname + "/../middlewares/currentUser");

var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
server.listen(4000);

module.exports = function (app) {

    io.on('connection', function (socket) {
        console.log('============');
        console.log('User connected');
        console.log('++++++++++++');
        console.log(socket.id);
        console.log('++++++++++++');
        socket.emit('start');
        socket.on('add-socket-id', function (user_id) {
            console.log('******');
            console.log(user_id);
            console.log('******');

            // socket.on('add-socket-id') when user is LogOut-???
            if(user_id !== null) {
                var User = mongoose.model("User");
                User.findOne({_id: user_id}).exec(function (err, user) {
                    if (err) {
                        console.log(err);
                    }
                    if (!user) {
                        console.log("Not found");
                    }
                    if (user.socketId === null)
                        user.socketId = [];
                    user.socketId.push(socket.id);
                    user.save(function (err) {
                        if (err) {
                            console.log(err);
                        }
                        // console.log(user);
                    })
                })
            }
            /*

            */

        });
        socket.on('disconnect', function() {
            console.log('User disconnected');
            console.log('#########');
            console.log(socket.id);
            console.log('#########');

            //if (socket.id !== null){
                var User = mongoose.model("User");
                User.findOne({socketId: socket.id}).exec(function (err, user) {
                    if (err) {
                        console.log(err);
                    }
                    if (!user) {
                        console.log("Not found");
                    }
                    else{
                        console.log('$$$$$$$$$$$$$$$$$$$$');
                        console.log(socket.id);
                        console.log(user);
                        console.log('$$$$$$$$$$$$$$$$$$$$');

                        var index = user.socketId.indexOf(socket.id);
                        user.socketId.splice(index, 1);
                        user.save(function (err) {
                            if (err) {
                                console.log(err);
                            }
                            console.log(user);
                        })

                    }
                })
           // }


        });
        socket.on('save-message', function (data /*, userPairSocketID*/) {
            console.log('=======');
            console.log(data);
            // console.log(userPairSocketID);
            console.log('=======');

            // TODO User.find({data.to}, function(err, user){
            // io.to(String(user.socketId)).emit('new-message', {message: data});
            // })

            var User = mongoose.model("User");
            User.findOne({_id: data.to}).exec(function (err, userTo) {
                if (err) {
                    console.log(err);
                }
                if (!userTo) {
                    console.log('----------');
                    console.log(data.to);
                   // console.log(user.socketId);
                    console.log('----------');
                    console.log("Not found");
                }
                else {
                    for (var i = 0; i < userTo.socketId.length; i++){
                        io.to(String(userTo.socketId[i])).emit('new-message', {message: data});
                    }
                    User.findOne({_id: data.from}).exec(function (err, userFrom) {
                        if (err) {
                            console.log(err);
                        }
                        if (!userFrom) {
                            console.log("Not found");
                        }
                        else {
                            for (var i = 0; i < userFrom.socketId.length; i++){
                                io.to(String(userFrom.socketId[i])).emit('new-message', {message: data});
                            }
                        }
                    });
                }
            });
        });
    });

    app.use("/chats", router);

    // Get messages from chat
    router.get('/', function(req, res) {
        var Messages = mongoose.model("Messages");
        var criteria = {
            $or: [
                {
                    from: req.query.user1,
                    to: req.query.user2
                },
                {
                    from: req.query.user2,
                    to: req.query.user1
                }
            ]
        }
        var query = Messages.find(criteria).sort({createdAt: -1});
        query.exec(function (err, messages) {
            if (err) {
                /* res.status(500); */
                return res.send(err);
            }
            if (!messages) {
                /* res.status(403); */
                return res.send("The chat is not exist");
            }
            console.log(messages);
            res.status(200).send(messages);
        })
    });

    // Create chat message
    router.post('/', function(req, res, next) {
        var Messages = mongoose.model("Messages");
        Messages.create(req.body, function (err, message) {
            if (err) return res.send(err);
            res.status(200).send(message);
        });
    });
}