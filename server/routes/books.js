var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var isLoggined = require(__dirname + "/../middlewares/isLoggined");
var currentUser = require(__dirname + "/../middlewares/currentUser");
var multer = require('multer');
var fs = require('fs');

storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, __dirname + '/../public/images');
    },
    filename: function(req, file, callback) {

        var filename = /*file.fieldname + '-' + */ Date.now() + '-' + file.originalname;
        req.body.finalFileName = filename;
        callback(null, filename);
    }
})

var upload = multer({ storage: storage });

module.exports = function (app) {

    app.use("/books", router);

    router.get('/', [isLoggined, currentUser], function(req, res, next) {
        var Book = mongoose.model("Book");
        console.log(req.query.currentcontrol);
        console.log(req.query.search);
        console.log(req.query.sort);
        console.log(JSON.parse(req.query.sort));
        let limit = 3;
        let skip = (req.query.currentcontrol - 1) * limit;
        var criteria = {};
        if (req.query.search) {
            criteria = {
                $or: [
                    {
                        "author": new RegExp(req.query.search, "i")
                    },
                    {
                        "title": new RegExp(req.query.search, "i")
                    }
                ]
            }
        }
        var query = Book.find(criteria).skip(skip).limit(limit);
        if (req.query.sort) {
            query.collation({locale: "en" }).sort(JSON.parse(req.query.sort));
        }

        query.exec(function (err, books) {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send(books);
        });
    });

    router.get('/count', [isLoggined, currentUser], function(req, res, next) {
        var Book = mongoose.model("Book");
        var criteria = {};
        if (req.query.search) {
            criteria = {
                $or: [
                    {
                        "author": new RegExp(req.query.search, "i")
                    },
                    {
                        "title": new RegExp(req.query.search, "i")
                    }
                ]
            }
        }
        Book.count(criteria, function (err, count) {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send({
                count: count
            });
        });
    });

    // Upload books files
    router.post('/files', upload.any(), [isLoggined, currentUser], function(req, res, next) {
        console.log(req.body);
        console.log(req.files);

        var TempFile = mongoose.model("TempFile");
        var TempFileData = {
            currentSignId: req.body.currentSignId,
            fileName: req.files[0].originalname,
            finalFileName: req.body.finalFileName,
            size: req.files[0].size,
            type: req.files[0].mimetype,
            path: req.files[0].path,
            time: +new Date
        }

        console.log(TempFileData);

        TempFile.create(TempFileData, function (err, createdTempFileData) {
            if (err) {
                var code = err.name === "ValidationError" ? 400 : 500;
                return res.status(code).send(err);
            }
            res.status(200).send(createdTempFileData);
        });

       /* res.status(200).json(req.files); */
    });

    // Books.create
    router.post('/', upload.fields([]), /*upload.single('bookImage'),*/ [isLoggined, currentUser], function(req, res, next) {

        var TempFile = mongoose.model("TempFile");
        var query = TempFile.find({ currentSignId: req.body.currentSignId });
        query.exec(function (err, tempFiles) {
            if (err) {
                return res.status(500).send(err);
            }
            console.log(tempFiles);
            var Book = mongoose.model("Book");
            var rand = 1 + Math.random() * 5;
            rand = Math.floor(rand);

            var bookData = {
                author: req.body.author,
                title: req.body.title,
                description: req.body.description,
                status: req.body.status,
                bookImageName: '-',
                attachments: tempFiles,
                bookRating: rand
            }

            Book.create(bookData, function (err, createdBook) {
                if (err) {
                    var code = err.name === "ValidationError" ? 400 : 500;
                    return res.status(code).send(err);
                }

                TempFile.remove({currentSignId: req.body.currentSignId}, function (err) {
                    if (err) {
                        console.log(err);
                    }
                    console.log("DELETED irrelevant data in TempFile");
                });

                res.status(200).send(createdBook);
            });
        });

    });
    router.put('/:id', upload.single('bookImage'), [isLoggined, currentUser], function(req, res, next) {
        var Book = mongoose.model("Book");
        Book.findOne({_id: req.params.id}).exec(function (err, book) {
            if (err) {
                return res.status(500).send(err);
            }
            // book.title.id nginx
            if (!book) {
                return res.status(404).send("Not found");
            }
            // var oldFile
            let bookImageNameOld = book.bookImageName;
            Book.update({_id: req.params.id}, {$set: req.body}, function (err, updatedResult) {
                if (err) {
                    return res.status(500).send("");
                }
                res.status(200).send(updatedResult);
                if( req.body.bookImage !== '' ){
                    fs.unlink(__dirname + '/../public/images/' + bookImageNameOld, function(error) {
                        if (error) {
                            throw error;
                        }
                        console.log('Image deleted');
                    });
                }
            })


        })
    });

    router.delete('/delete', [isLoggined, currentUser], function(req, res, next) {
        var Book = mongoose.model("Book");
        Book.findOne({_id: req.query.id}).exec(function (err, book) {
            let bookImageName = book.bookImageName;
            console.log(bookImageName);


            Book.remove({_id: req.query.id}, function (err) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.status(200).send("DELETED");

                fs.unlink(__dirname + '/../public/images/' + bookImageName, function(error) {
                    if (error) {
                        throw error;
                    }
                    console.log('Image deleted');
                });
            })

        });

    });
};