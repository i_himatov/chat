var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var isLoggined = require(__dirname + "/../middlewares/isLoggined");
var currentUser = require(__dirname + "/../middlewares/currentUser");

var jwt = require('jwt-express');

var createUser = require('../userFactory/createUser');
var createSocialUser = require('../userFactory/createSocialUser');
var {google} = require('googleapis');
var plus = google.plus('v1');
var OAuth2 = google.auth.OAuth2;
var oauth2Client = new OAuth2(
    '587806090200-g2d9uhfgos5f6qhapscvoadsg43t8dcs.apps.googleusercontent.com',
    'MeYMGNvqAeBUf_2QuLVtHk8g',
    'http://127.0.0.1:4200/'
);


module.exports = function (app) {

    app.use("/user", router);

    router.get('/google', function(req, res, next) {
        console.log('google');
// generate a url that asks permissions for Google+ and Google Calendar scopes
        var scopes = [
            'https://www.googleapis.com/auth/plus.me',
            'https://www.googleapis.com/auth/userinfo.email'
        ];

        var url = oauth2Client.generateAuthUrl({
            // 'online' (default) or 'offline' (gets refresh_token)
            access_type: 'offline',

            // If you only need one scope you can pass it as a string
            scope: scopes,

            // Optional property that passes state parameters to redirect URI
            // state: 'foo'
        });
        console.log(url);
        res.status(200).send(url);
    });

    router.get('/google/gettoken', function(req, res, next) {
        console.log('code');
        console.log(req.query.code);
        oauth2Client.getToken(String(req.query.code), function (err, tokens) {
            // Now tokens contains an access_token and an optional refresh_token. Save them.
            if (!err) {
                console.log('tokens');
                console.log(tokens);
                oauth2Client.setCredentials(tokens);
                plus.people.get({
                    userId: 'me',
                    auth: oauth2Client
                }, function (err, response) {
                    if (err) {
                        return res.status(500).send(err);
                    }
                    else {
                       // console.log(response);
                       // console.log(response.data);
                        var SocialUser = mongoose.model("SocialUser");
                        SocialUser.findOne({socialId: response.data.id}).exec(function (err, SocialUser) {
                            if (err) {
                                return res.status(500).send(err);
                            }
                            else if (SocialUser) {
                                var User = mongoose.model("User");
                                User.findOne({_id: SocialUser.user_id}).exec(function (err, user) {
                                    if (err) {
                                        return res.status(500).send(err);
                                    }
                                    else {
                                        return res.status(200).send({
                                            token:  res.jwt({
                                                id: user.id
                                            }).token,
                                            email: user.email
                                        });
                                    }
                                });
                            }
                            else {
                                console.log('=========');
                                console.log(req.jwt);
                                console.log(req.jwt.payload);
                                console.log(req.jwt.payload.id);
                                console.log('=========');
                                if (req.jwt && req.jwt.valid && req.jwt.payload && req.jwt.payload.id) {
                                    console.log('fgdgdghdgfddgfdfffggfffdgfdfgdgfdfgdtrdtr');
                                    var User = mongoose.model("User");
                                    User.findOne({_id: req.jwt.payload.id}).exec(function (err, user) {
                                        if (err) {
                                            return res.status(500).send(err);
                                        }
                                        else if(user){
                                            console.log('88888888888888888888888888888888888');
                                            createSocialUser(tokens, user, response.data.id, function (createdSocialUser) {
                                                res.status(200).send({
                                                    token:  res.jwt({
                                                        id: createdSocialUser.user_id
                                                    }).token,
                                                    email: createdSocialUser.email
                                                });
                                            }, function (err, code) {
                                                return res.status(code).send(err);
                                            });
                                        }
                                        else if(!user) {
                                            console.log('JWT token is invalid');
                                            return res.status(401).send(err);
                                        }
                                    });

                                }
                                else {
                                    // Check if user is already registered but Logout
                                    console.log('Check if user is already registered but Logout');
                                    var User = mongoose.model("User");
                                    User.findOne({email: response.data.emails[0].value}).exec(function (err, user) {
                                        if (err) {
                                            return res.status(500).send(err);
                                        }
                                        else if (user) {
                                            console.log('User is already registered but Logout');
                                            // User is already registered but Logout
                                            createSocialUser(tokens, user, response.data.id, function (createdSocialUser) {
                                                res.status(200).send({
                                                    token:  res.jwt({
                                                        id: createdSocialUser.user_id
                                                    }).token,
                                                    email: createdSocialUser.email
                                                });
                                            }, function (err, code) {
                                                return res.status(code).send(err);
                                            });
                                        }
                                        else{
                                            console.log('User doesn\'t registered');
                                            // User doesn't registered
                                            // create User
                                            // create SocialUser
                                            // return JWT
                                            createUser(response.data.displayName, response.data.emails[0].value, function (createdUser, password) {
                                                if (err) {
                                                    var code = err.name === "ValidationError" ? 400 : 500;
                                                    return res.status(code).send(err);
                                                }
                                                createSocialUser(tokens, createdUser, response.data.id, function (createdSocialUser) {
                                                    res.status(200).send({
                                                        token:  res.jwt({
                                                            id: createdSocialUser.user_id
                                                        }).token,
                                                        email: createdSocialUser.email
                                                    });
                                                }, function (err, code) {
                                                    return res.status(code).send(err);
                                                });
                                                createdUser.sendPassword(password);
                                            });
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            }
            else {
                console.log(err);
            }
        });
    });
    /*
    router.get('/google/code', function(req, res, next) {
        console.log(req.query.code);
    });
    */

    router.get('/', [isLoggined, currentUser], function(req, res, next) {
        var User = mongoose.model("User");
        var query = User.find({});
        /*
        if (req.query.limit) {
            query.limit(+req.query.limit);
        }
        if (req.query.skip) {
            query.skip(+req.query.skip);
        }
        */
        query.exec(function (err, users) {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send(users);
        });
    });

    router.get('/count', [isLoggined, currentUser], function(req, res, next) {
        var User = mongoose.model("User");
        User.count({}, function (err, count) {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send({
                count: count
            });
        });
    });

    router.get('/:id', [isLoggined, currentUser], function(req, res, next) {
        var User = mongoose.model("User");
        User.findOne({_id: req.params.id}).exec(function (err, user) {
            if (err) {
                return res.status(500).send(err);
            }
            if (!user) {
                return res.status(404).send("Not found");
            }
            res.status(200).send(user);
        })
    });


    // Write resetToken in DB, send the letter
    router.get('/resetpassword/:email', function(req, res, next) {
        var User = mongoose.model("User");
        User.findOne({email: req.params.email}).exec(function (err, user) {
            if (err) {
                return res.status(500).send(err);
            }
            if (!user) {
                /* res.status(404) */
                return res.send("Not found");
            }

            user.sendResetPasswordEmail(res.jwt, function(err) {
                if(err){
                    console.log('Error');
                    console.log(err);
                    res.status(500).send(err);
                } else {
                    console.log('Email Sent');
                    res.status(200).send('Email Sent');
                }
            });


            //TODO: move code


            // nodemailer
                // res - check e-mail
                // parse link
                // redirect to change password page || login
                // save new password && delete token
            /* res.status(200).send(user); */
        });
    });

    router.get('/email/:email', function(req, res, next) {
        var User = mongoose.model("User");
        User.findOne({email: req.params.email}).exec(function (err, user) {
            if (err) {
                return res.status(500).send(err);
            }
            if (!user) {
                /* res.status(404) */
                return res.send("Not found");
            }
            res.status(200).send(user);
        })
    });

    // Get user by reset token after user clicked the link in a letter
    router.get('/token/:resettoken', function(req, res, next) {
        var User = mongoose.model("User");
        User.findOne({resetToken: req.params.resettoken}).exec(function (err, user) {
            if (err) {
                return res.status(500).send(err);
            }
            if (!user) {
                /* res.status(404) */
                return res.send("Not found");
            }
            res.status(200).send(user);
        })
    });

    // Update password

    router.put('/resetpassword/:id', function(req, res, next) {
        var User = mongoose.model("User");
        console.log(req.body);
        User.findOne({_id: req.params.id}).exec(function (err, user) {
            if (err) {
                return res.status(500).send(err);
            }
            if (!user) {
                return res.status(404).send("Not found");
            }
            user.password = req.body.password2;
            user.resetToken = "";
            user.save(function (err) {
                if (err) {
                    return res.status(500).send(err);
                }
                res.status(200).send(user);
            })
        });
    });

    // User.create
    router.post('/', function(req, res, next) {
        var User = mongoose.model("User");
        User.create(req.body, function (err, createdUser) {
            if (err) {
                var code = err.name === "ValidationError" ? 400 : 500;
                return res.status(code).send(err);
            }
            res.status(200).send(createdUser);
        });
    });
    router.post('/:id', [isLoggined, currentUser], function(req, res, next) {
        var User = mongoose.model("User");
        User.findOne({_id: req.params.id}).exec(function (err, user) {
            if (err) {
                return res.status(500).send(err);
            }
            if (!user) {
                return res.status(404).send("Not found");
            }
            User.update({_id: req.params.id}, {$set: req.body}, function (err, updatedResult) {
                if (err) {
                    return res.status(500).send("")
                }
                res.status(200).send(updatedResult);
            })
        })
    });

    router.delete('/:id', [isLoggined, currentUser], function(req, res, next) {
        var User = mongoose.model("User");
        User.remove({_id: req.params.id}, function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send("DELETED");
        })
    });
};