module.exports = function (req, res, next) {
    if (req.jwt && req.jwt.valid && req.jwt.payload && req.jwt.payload.id) {
/*
        console.log('JWT debug');
        console.log(req.jwt);
        console.log(req.jwt.valid);
        console.log(req.jwt.payload);
        console.log(req.jwt.payload.id);
        */
        next();
    } else {
        res.status(403).send("Forbiden");
    }

};